package ru.tsc.denisturovsky.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.util.DateUtil;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "projectDTO", propOrder = {
        "id",
        "name",
        "description",
        "status",
        "created",
        "createdBy",
        "updated",
        "updatedBy",
        "dateBegin",
        "dateEnd",
        "userId"
})
@EntityListeners(AuditingEntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class ProjectDTO {

    private static final long serialVersionUID = 1;

    @Id
    @NotNull
    @XmlElement(required = true)
    private String id = UUID.randomUUID().toString();

    @Column
    @NotNull
    @XmlElement(required = true)
    private String name = "";

    @Column
    @NotNull
    @XmlElement(required = true)
    private String description = "";

    @Column
    @NotNull
    @XmlElement(required = true)
    @Enumerated(EnumType.STRING)
    @XmlSchemaType(name = "string")
    private Status status = Status.NOT_STARTED;

    @Column
    @NotNull
    @CreatedDate
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date created = new Date();


    @NotNull
    @CreatedBy
    @XmlElement(required = true)
    @Column(name = "created_by")
    private String createdBy = "";

    @Column
    @NotNull
    @LastModifiedDate
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date updated = new Date();

    @NotNull
    @LastModifiedBy
    @XmlElement(required = true)
    @Column(name = "updated_by")
    private String updatedBy = "";

    @Nullable
    @Column(name = "user_id")
    @XmlElement(required = true)
    private String userId;

    @Nullable
    @Column(name = "date_begin")
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateBegin;

    @Nullable
    @Column(name = "date_end")
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateEnd;

    public ProjectDTO(@NotNull final String name) {
        this.name = name;
    }

    public ProjectDTO(
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.name = name;
        this.description = description;
    }

    public ProjectDTO(
            @NotNull final String name,
            @NotNull final Status status
    ) {
        this.name = name;
        this.status = status;
    }

    public ProjectDTO(
            @NotNull final String name,
            @NotNull final Status status,
            @NotNull final Date dateBegin,
            @NotNull final Date dateEnd
    ) {
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }

    public ProjectDTO(
            @NotNull final String name,
            @NotNull final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
        this.description = description;
        this.name = name;
    }

    @NotNull
    @Override
    public String toString() {
        return String.format(
                "%30s:%30s:%30s:%30s:%30s|",
                name,
                getStatus().getDisplayName(),
                description,
                DateUtil.toString(getDateBegin()),
                DateUtil.toString(getDateEnd())
        );
    }

}

