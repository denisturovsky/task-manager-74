package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.tsc.denisturovsky.tm.model.Project;
import ru.tsc.denisturovsky.tm.model.Task;
import ru.tsc.denisturovsky.tm.model.User;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface TaskRepository extends JpaRepository<Task, String> {

    long countByUser(@NotNull final User user);

    void deleteByUser(@NotNull final User user);

    void deleteByUserAndId(
            @NotNull final User user,
            @NotNull final String id
    );

    @Query("select case when count(c)> 0 then true else false end from Task c where user.id = :userId and id = :id")
    boolean existByUserIdAndId(
            @Param("userId") String userId,
            @Param("id") String id
    );

    @NotNull
    List<Task> findByProject(@NotNull final Project project);

    @NotNull
    List<Task> findByUser(@NotNull final User user);

    @NotNull
    List<Task> findByUser(
            @NotNull final User user,
            @NotNull final Sort sort
    );

    @NotNull
    Optional<Task> findByUserAndId(
            @NotNull final User user,
            @NotNull final String id
    );

    @NotNull
    List<Task> findByUserAndProjectId(
            @NotNull final User user,
            @NotNull final String projectId
    );

}
