package ru.tsc.denisturovsky.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectDTOService;
import ru.tsc.denisturovsky.tm.dto.soap.*;
import ru.tsc.denisturovsky.tm.util.UserUtil;

@Endpoint
public class ProjectSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";

    public final static String NAMESPACE = "http://denisturovsky.tsc.ru/tm/dto/soap";

    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    @Autowired
    private IProjectDTOService projectService;

    @ResponsePayload
    @PayloadRoot(localPart = "projectAddRequest", namespace = NAMESPACE)
    public ProjectAddResponse add(@RequestPayload final ProjectAddRequest request) throws Exception {
        return new ProjectAddResponse(projectService.addByUserId(UserUtil.getUserId(), request.getProject()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteAllRequest", namespace = NAMESPACE)
    public ProjectDeleteAllResponse clear(@RequestPayload final ProjectDeleteAllRequest request) throws Exception {
        projectService.clearByUserId(UserUtil.getUserId());
        return new ProjectDeleteAllResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectCountRequest", namespace = NAMESPACE)
    public ProjectCountResponse count(@RequestPayload final ProjectCountRequest request) throws Exception {
        return new ProjectCountResponse(projectService.countByUserId(UserUtil.getUserId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    public ProjectDeleteResponse delete(@RequestPayload final ProjectDeleteRequest request) throws Exception {
        projectService.removeByUserId(UserUtil.getUserId(), request.getProject());
        return new ProjectDeleteResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(@RequestPayload final ProjectDeleteByIdRequest request) throws Exception {
        projectService.removeByUserIdAndId(UserUtil.getUserId(), request.getId());
        return new ProjectDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectExistsByIdRequest", namespace = NAMESPACE)
    public ProjectExistsByIdResponse existsById(@RequestPayload final ProjectExistsByIdRequest request) throws Exception {
        return new ProjectExistsByIdResponse(projectService.existsByUserIdAndId(UserUtil.getUserId(), request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(@RequestPayload final ProjectFindAllRequest request) throws Exception {
        return new ProjectFindAllResponse(projectService.findAllByUserId(UserUtil.getUserId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findById(@RequestPayload final ProjectFindByIdRequest request) throws Exception {
        return new ProjectFindByIdResponse(projectService.findOneByUserIdAndId(UserUtil.getUserId(), request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse save(@RequestPayload final ProjectSaveRequest request) throws Exception {
        return new ProjectSaveResponse(projectService.updateByUserId(UserUtil.getUserId(), request.getProject()));
    }

}