package ru.tsc.denisturovsky.tm.api.endpoint;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.model.Result;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/api/auth")
public interface AuthEndpoint {

    @WebMethod
    @PostMapping("/login")
    Result login(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password
    );

    @WebMethod
    @PostMapping("/logout")
    Result logout();

    @WebMethod
    @GetMapping("/profile")
    UserDTO profile();

}