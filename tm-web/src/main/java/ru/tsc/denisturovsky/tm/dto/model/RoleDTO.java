package ru.tsc.denisturovsky.tm.dto.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.enumerated.RoleType;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tm_role")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "roleDTO", propOrder = {
        "id",
        "userId",
        "roleType"
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class RoleDTO {

    @Id
    @Column
    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column(name = "user_id")
    private String userId;

    @NotNull
    @Column(name = "role_type")
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    @NotNull
    @Override
    public String toString() {
        return roleType.name();
    }

}