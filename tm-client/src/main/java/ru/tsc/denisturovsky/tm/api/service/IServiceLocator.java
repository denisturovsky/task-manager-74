package ru.tsc.denisturovsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    IAuthEndpoint getAuthEndpointClient();

    @NotNull
    IClientLoggerService getLoggerService();

    @NotNull
    IProjectEndpoint getProjectEndpointClient();

    @NotNull
    IClientPropertyService getPropertyService();

    @NotNull
    ISystemEndpoint getSystemEndpointClient();

    @NotNull
    ITaskEndpoint getTaskEndpointClient();

    @NotNull
    ITokenService getTokenService();

    @NotNull
    IUserEndpoint getUserEndpointClient();

}
