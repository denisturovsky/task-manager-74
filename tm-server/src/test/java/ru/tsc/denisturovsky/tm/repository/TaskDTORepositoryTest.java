package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.IUserDTOService;
import ru.tsc.denisturovsky.tm.dto.model.TaskDTO;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.marker.UnitCategory;
import ru.tsc.denisturovsky.tm.repository.dto.TaskDTORepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static ru.tsc.denisturovsky.tm.constant.ContextTestData.CONTEXT;
import static ru.tsc.denisturovsky.tm.constant.ProjectTestData.USER_PROJECT1;
import static ru.tsc.denisturovsky.tm.constant.TaskTestData.*;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class TaskDTORepositoryTest {

    @NotNull
    private final static IUserDTOService USER_SERVICE = CONTEXT.getBean(IUserDTOService.class);

    @NotNull
    private final static IProjectDTOService PROJECT_SERVICE = CONTEXT.getBean(IProjectDTOService.class);

    @NotNull
    private final static TaskDTORepository REPOSITORY = CONTEXT.getBean(TaskDTORepository.class);

    @NotNull
    private static String USER_ID = "";

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final UserDTO user = USER_SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        USER_ID = user.getId();
        PROJECT_SERVICE.add(USER_ID, USER_PROJECT1);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        PROJECT_SERVICE.remove(USER_ID, USER_PROJECT1);
        if (user != null) USER_SERVICE.remove(user);
    }

    @Test
    public void addByUserId() throws Exception {
        USER_TASK3.setUserId(USER_ID);
        Assert.assertNotNull(REPOSITORY.save(USER_TASK3));
        @Nullable final Optional<TaskDTO> task = REPOSITORY.findByUserIdAndId(USER_ID, USER_TASK3.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK3.getId(), task.get().getId());
        Assert.assertEquals(USER_ID, task.get().getUserId());
    }

    @After
    public void after() throws Exception {
        REPOSITORY.deleteAll();
    }

    @Before
    public void before() throws Exception {
        USER_TASK1.setUserId(USER_ID);
        USER_TASK2.setUserId(USER_ID);
        REPOSITORY.save(USER_TASK1);
        REPOSITORY.save(USER_TASK2);
    }

    @Test
    public void clearByUserId() throws Exception {
        REPOSITORY.deleteByUserId(USER_ID);
        Assert.assertEquals(0, REPOSITORY.findByUserId(USER_ID).size());
    }

    @Test
    public void createByUserId() throws Exception {
        USER_TASK3.setUserId(USER_ID);
        @NotNull final TaskDTO task = REPOSITORY.save(USER_TASK3);
        Assert.assertEquals(USER_TASK3.getName(), task.getName());
        Assert.assertEquals(USER_ID, task.getUserId());
    }

    @Test
    public void createByUserIdWithDescription() throws Exception {
        USER_TASK3.setUserId(USER_ID);
        @NotNull final TaskDTO task = REPOSITORY.save(USER_TASK3);
        Assert.assertEquals(USER_TASK3.getName(), task.getName());
        Assert.assertEquals(USER_TASK3.getDescription(), task.getDescription());
        Assert.assertEquals(USER_ID, task.getUserId());
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertFalse(REPOSITORY.existByUserIdAndId(USER_ID, NON_EXISTING_TASK_ID));
        Assert.assertTrue(REPOSITORY.existByUserIdAndId(USER_ID, USER_TASK1.getId()));
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertEquals(Collections.emptyList(), REPOSITORY.findByUserId(""));
        final List<TaskDTO> tasks = REPOSITORY.findByUserId(USER_ID);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        tasks.forEach(task -> Assert.assertEquals(USER_ID, task.getUserId()));
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertEquals(Optional.empty(), REPOSITORY.findByUserIdAndId(USER_ID, NON_EXISTING_TASK_ID));
        @Nullable final Optional<TaskDTO> task = REPOSITORY.findByUserIdAndId(USER_ID, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.get().getId());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertEquals(2, REPOSITORY.findByUserId(USER_ID).size());
    }

    @Test
    public void removeByUserId() throws Exception {
        REPOSITORY.deleteByUserIdAndId(USER_ID, USER_TASK2.getId());
        Assert.assertEquals(Optional.empty(), REPOSITORY.findByUserIdAndId(USER_ID, USER_TASK2.getId()));
    }

    @Test
    public void update() throws Exception {
        USER_TASK1.setName(USER_TASK3.getName());
        REPOSITORY.save(USER_TASK1);
        Assert.assertEquals(USER_TASK3.getName(), REPOSITORY.findByUserIdAndId(USER_ID, USER_TASK1.getId()).get()
                                                            .getName());
    }

}