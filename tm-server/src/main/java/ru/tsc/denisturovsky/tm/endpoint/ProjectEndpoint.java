package ru.tsc.denisturovsky.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.denisturovsky.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectDTOService;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;
import ru.tsc.denisturovsky.tm.dto.model.SessionDTO;
import ru.tsc.denisturovsky.tm.dto.request.*;
import ru.tsc.denisturovsky.tm.dto.response.*;
import ru.tsc.denisturovsky.tm.enumerated.CustomSort;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.exception.EndpointException;
import ru.tsc.denisturovsky.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.denisturovsky.tm.sevice.dto.ProjectTaskDTOService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.tsc.denisturovsky.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Autowired
    private ProjectTaskDTOService projectTaskService;

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIdResponse changeStatusByIdProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        try {
            projectService.changeProjectStatusById(userId, id, status);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectChangeStatusByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectClearRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        try {
            assert userId != null;
            projectService.clear(userId);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCompleteByIdResponse completeByIdProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCompleteByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        try {
            projectService.changeProjectStatusById(userId, id, Status.COMPLETED);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectCompleteByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCreateRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Date dateBegin = request.getDateBegin();
        @Nullable final Date dateEnd = request.getDateEnd();
        @NotNull ProjectDTO project;
        try {
            if (dateBegin == null || dateEnd == null) {
                if (description == null) {
                    project = projectService.create(userId, name);
                } else {
                    project = projectService.create(userId, name, description);
                }
            } else {
                project = projectService.create(userId, name, description, dateBegin, dateEnd);
            }
            return new ProjectCreateResponse(project);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectListResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectListRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final CustomSort sort = request.getSort();
        try {
            @NotNull final List<ProjectDTO> projects = projectService.findAll(userId, sort);
            return new ProjectListResponse(projects);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIdResponse removeByIdProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final ProjectDTO project;
        try {
            assert userId != null;
            assert id != null;
            project = projectService.findOneById(userId, id);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        if (project == null) throw new ProjectNotFoundException();
        try {
            projectTaskService.removeProjectById(userId, project.getId());
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectShowByIdResponse showByIdProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        try {
            assert userId != null;
            assert id != null;
            @Nullable final ProjectDTO project = projectService.findOneById(userId, id);
            return new ProjectShowByIdResponse(project);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectStartByIdResponse startByIdProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectStartByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        try {
            projectService.changeProjectStatusById(userId, id, Status.IN_PROGRESS);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectStartByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIdResponse updateByIdProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        try {
            projectService.updateOneById(userId, id, name, description);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectUpdateByIdResponse();
    }

}